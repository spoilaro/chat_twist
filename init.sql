-- Database structure for Chat Twist POC

drop table if exists message_types cascade;
create table message_types (
	type_id UUID,
	message_type TEXT unique not null,
	PRIMARY KEY(type_id)
);

drop table if exists messages;
create table messages (
	message_id UUID,
	fk_message_type UUID,
	message_content TEXT not null,
	PRIMARY KEY(message_id),
	FOREIGN KEY(fk_message_type) REFERENCES message_types(type_id)
);

-- Message types
insert into message_types (type_id, message_type) values (gen_random_uuid(), 'INITIAL');
insert into message_types (type_id, message_type) values (gen_random_uuid(), 'NORMAL');


create or replace procedure add_initial_message("msg" text)
language sql
as $$
-- Initial messages
insert into messages (
	message_id,
	fk_message_type,
	message_content
) values (
	gen_random_uuid(),
	(select type_id from message_types where message_type = 'INITIAL' limit 1),
	msg
);
$$;

call add_initial_message('I got a new puppy');
call add_initial_message('My car broke down');
call add_initial_message('My boss yelled at me');

create or replace function get_initial_messages("player_count" integer)
returns setof messages
language sql
as $$
	select message_id, messages.fk_message_type, message_content from messages
	inner join message_types on message_types.type_id=messages.fk_message_type
	where message_types.message_type = 'INITIAL'
	order by random()
	limit player_count;
$$;
